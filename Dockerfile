FROM rockylinux:8
USER root

RUN curl -sL https://dl.yarnpkg.com/rpm/yarn.repo | tee /etc/yum.repos.d/yarn.repo
RUN yum -y install epel-release wget git
RUN yum -y update
RUN dnf --enablerepo=powertools -y install perl-IPC-Run perl-IPC-Cmd 
RUN dnf install -y ruby-devel
RUN dnf --enablerepo=devel -y install libidn-devel xmlsec1-devel xmlsec1-openssl-devel

RUN yum -y install \
    autoconf \
    automake \
    ca-certificates \
    gcc \
    gcc-c++ \
    gdbm \
    gdbm-devel \
    git \
    glibc \
    libcurl-devel \
    libffi \
    libffi-devel \
    libstdc++ \
    libtool \
    libtool-ltdl \
    libtool-ltdl-devel \
    libxml2-devel \
    libxslt-devel \
    make \
    openssl \
    openssl-devel \
    rpm-build \
    rpmdevtools \
    xmlsec1 \
    xmlsec1-openssl \
    zlib \
    zlib-devel \
 && yum clean all

 RUN git clone https://github.com/rbenv/ruby-build.git && \
  PREFIX=/usr/local ./ruby-build/install.sh && \
  ruby-build -v 3.1.1 /usr/local

RUN gem install -N fpm-cookery

SHELL ["/bin/bash", "-lc"]
CMD ["/bin/bash", "-l"]
